import zBuilder
import zBuilder.builder
import zBuilder.updates as updates
import json
import logging

logger = logging.getLogger(__name__)


class BaseNodeEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, '_class'):
            if hasattr(obj, 'serialize'):
                return obj.serialize()
            else:
                return obj.__dict__
        else:
            return super(BaseNodeEncoder, self).default(obj)


def pack_zbuilder_contents(builder, type_filter, invert_match):
    """ Utility to package the data in a dictionary.
    """
    logger.info("packing zBuilder contents for json.")

    node_data = dict()
    node_data['d_type'] = 'node_data'
    node_data['data'] = builder.get_scene_items(type_filter=type_filter, invert_match=invert_match)

    info = dict()
    info['d_type'] = 'info'
    info['data'] = builder.info

    return [node_data, info]


def unpack_zbuilder_contents(builder, json_data):
    """ Gets data out of json serialization and assigns it to node collection
    object.

    Args:
        json_data: Data to assign to builder object.
    """
    for d in json_data:
        if d['d_type'] == 'node_data':
            builder.bundle.extend_scene_items(d['data'])
            logger.info("Reading scene items. {} nodes".format(len(d['data'])))

        if d['d_type'] == 'info':
            builder.info = d['data']
            logger.info("Reading info.")

    logger.info("Assigning builder.")
    for item in builder.bundle:
        if item:
            item.builder = builder


def load_base_node(json_object):
    """
    Loads json objects into proper classes.  Serves as object hook for loading
    json.

    Args:
        json_object (obj): json obj to perform action on

    Returns:
        obj:  Result of operation
    """
    if '_class' in json_object:
        type_ = json_object.get('type', 'Base')
        builder_type = json_object['_builder_type']
        obj = zBuilder.builder.find_class(builder_type, type_)
        check_disk_version(json_object)

        # this catches the scene items for ui that slip in.
        try:
            scene_item = obj()
            scene_item.deserialize(json_object)

            return scene_item
        except TypeError:
            return json_object
    else:
        return json_object


def check_disk_version(json_object):
    """This checks the library version of the passed builder object to check if 
    it needs to be updated.

    Args:
        builder (obj): The builder object to check version.
    """
    # pre 1.0.11 we need to parameter reference to each node
    one_ten = '1.0.10'.split('.')
    one_ten = [int(v) for v in one_ten]

    json_version = json_object['info']['version'].split('.')
    json_version = [int(v) for v in json_version]

    if json_version <= one_ten:
        updates.update_json_pre_1_0_11(json_object)
